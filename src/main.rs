extern crate ezing;
extern crate futures;
extern crate quicksilver;
extern crate rand;

use std::collections::HashMap;
use std::time::Instant;

use futures::{future, Future};
use quicksilver::{
    geom::{Rectangle, Transform, Vector},
    graphics::{
        Color, Draw, Image, ImageScaleStrategy, ResizeStrategy, View, Window,
        WindowBuilder,
    },
    input::{ButtonState, Event, Key, MouseButton},
    run,
    sound::Sound,
    State,
};
use rand::{thread_rng, Rng};

const SIZE: (u32, u32) = (480, 320);
const OFFSET_LV: Vector = Vector { x: 16.0, y: -16.0 };
const OFFSET_R: u32 = SIZE_L.0;
const OFFSET_RV: Vector = Vector {
    x: OFFSET_R as f32,
    y: 0.0,
};

const SIZE_R: (u32, u32) = (SIZE.0 - OFFSET_R, SIZE.1);
const SIZE_L: (u32, u32) = ((BLOCK_SIZE * TETRIS_WIDTH as f32) as u32, SIZE.1);
const PLAYER_POS: Vector = Vector {
    x: SIZE_R.0 as f32 / 2.0,
    y: SIZE.1 as f32 / 2.0,
};
const GUN_OFFSET: Vector = Vector { x: 4.0, y: -4.0 };
const GUN_SHOOT_OFFSET: Vector = Vector { x: 46.0, y: 10.5 };
const GUN_CENTER: Vector = Vector { x: -9.0, y: -11.0 };
const LASER_DURATION: f32 = 0.5;
const LASER_WIDTH: f32 = 2.0;
const ROBOT_SPEED: f32 = 7.0;
const ROBOT_SPEED_MAX: f32 = 60.0;
const ROBOT_SLOW_TIME: f32 = 8.0;
const ROBOT_OUT_OF_SPACE: f32 = 3.0;
const ROBOT_RADIUS: f32 = 20.0;
const ROBOT_SPAWN_INTERVAL: f32 = 20.0;
const ROBOT_WAVE_SIZE: (usize, usize) = (6, 10);
const ROBOT_SPAWN_DISTANCE: (f32, f32) =
    (SIZE_R.0 as f32 * 0.75, SIZE_R.0 as f32 * 1.0);
const TETRIS_WIDTH: usize = 10;
const TETRIS_HEIGHT: usize = 18;
const TETRIS_MOVE_INTERVAL: f32 = 0.3;
const TIMESCALE_FAST: f32 = 9.0;
const BLOCK_SIZE: f32 = 16.0;

const COMPACT_TIME: f32 = 2.0;
const RETRACT_TIME: f32 = 1.0;
const ROBOT_DEATH_TIME: f32 = 3.0;
const FADE_OUT_START: f32 = 4.0;
const FADE_OUT_TIME: f32 = 2.0;
const SHAKE_START: f32 = 1.0;
const SHAKE_END: f32 = 2.5;
const SHAKE_FADE_IN: f32 = 0.7;
const SHAKE_FADE_OUT: f32 = 1.0;
const SHAKE_INTENSITY: f32 = 60.0;
const SHAKE_INTENSITY_ROT: f32 = 18.0;
const SHAKE_FREQUENCY: f32 = 40.0;

#[derive(Copy, Clone, Debug, Eq, PartialEq)]
enum Space {
    Filled {
        rot: u8,
        image: usize,
        size: (u8, u8),
        index: (u8, u8),
    },
    Empty,
}

#[derive(Copy, Clone, Debug, Eq, PartialEq)]
struct TetrisShape(u8);

#[derive(Copy, Clone, Debug)]
struct TetrisPiece {
    x: i32,
    y: i32,
    rot: u8,
    shape: TetrisShape,
}

struct Robot {
    position: Vector,
    velocity: Vector,
    image: usize,
    frame: usize,
    time: f32,
    angle: f32,
    treads: Option<usize>,
}

struct Animation {
    image: Image,
    frames: usize,
    frame_offset: usize,
    timer: f32,
    frame_time: f32,
}

enum Row {
    Incomplete(Vec<Space>),
    Compacting { spaces: Vec<Space>, time: f32 },
    Retracting { spaces: Vec<Space>, time: f32 },
}

#[derive(Copy, Clone)]
struct ScreenShake {
    next: (Vector, f32),
    last: (Vector, f32),
    time: f32,
}

struct Laser {
    from: Vector,
    to: Vector,
    time: f32,
}

#[derive(Copy, Clone)]
enum GameState {
    Playing,
    DeadRobot {
        time: f32,
        pos: Vector,
        shake: Option<ScreenShake>,
    },
}

struct PlayState {
    last_tick: Instant,
    player_direction: Vector,
    player_angle: f32,
    next_robot: u32,
    robots: HashMap<u32, Robot>,
    robot_spawn_timer: f32,
    tetris_grid: Vec<Row>,
    tetris_move_timer: f32,
    tetris_piece: Option<TetrisPiece>,
    lasers: Vec<Laser>,
    out_of_space: f32,
    bots_beaned: usize,
    mass_consumed: usize,

    fast_held: bool,
    background_image: Image,
    numbers_sheet: Animation,
    desert_image: Image,
    gameover_image: Image,
    piece_images: Vec<Image>,
    ghost_images: Vec<Image>,
    robot_anims: Vec<Animation>,
    tread_anim: Animation,
    player_anim: Animation,
    gun_anim: Animation,
    gun_stop: Image,
    sound_crush: Sound,
    sound_gameover: Sound,
    squisher: Image,

    state: GameState,
}

fn lerp(a: f32, b: f32, f: f32) -> f32 {
    a * (1.0 - f) + b * f
}

fn rgb(r: u8, g: u8, b: u8) -> Color {
    Color {
        r: r as f32 / 255.0,
        g: g as f32 / 255.0,
        b: b as f32 / 255.0,
        a: 1.0,
    }
}

impl ScreenShake {
    fn new() -> ScreenShake {
        let mut rng = thread_rng();
        let angle = rng.gen_range(0.0, 360.0);
        let dist = rng.gen_range(0.0, 1.0);
        let next = Vector::from_angle(angle) * dist;
        let angle = rng.gen_range(-1.0, 1.0);
        ScreenShake {
            next: (next, angle),
            last: (Vector::zero(), 0.0),
            time: 0.0,
        }
    }

    fn update(&mut self, dt: f32, frequency: f32) {
        self.time -= dt;
        let mut rng = thread_rng();
        while self.time < 0.0 {
            self.time += 1.0 / frequency;
            self.last = self.next;
            let angle = rng.gen_range(0.0, 360.0);
            let dist = rng.gen_range(0.0, 1.0);
            let rot = rng.gen_range(-1.0, 1.0);
            self.next = (Vector::from_angle(angle) * dist, rot);
        }
    }

    fn offset(&self, frequency: f32) -> (Vector, f32) {
        let x = lerp(self.next.0.x, self.last.0.x, self.time * frequency);
        let y = lerp(self.next.0.y, self.last.0.y, self.time * frequency);
        let rot = lerp(self.next.1, self.last.1, self.time * frequency);
        (Vector::new(x, y), rot)
    }
}

impl Animation {
    fn new(path: &'static str, frames: usize, frame_time: f32) -> Animation {
        Animation {
            image: Image::load(path).wait().unwrap(),
            frames,
            frame_offset: 0,
            timer: 0.0,
            frame_time,
        }
    }

    fn update(&mut self, dt: f32) {
        self.timer -= dt;
        while self.timer < 0.0 {
            self.timer += self.frame_time;
            self.frame_offset += 1;
        }
    }

    fn get_frame(&self, frame: usize) -> Image {
        let frame = (frame + self.frame_offset) % self.frames;
        let rect = Rectangle::new(
            (frame as f32 * self.image.area().width) / self.frames as f32,
            0.0,
            self.image.area().width / self.frames as f32,
            self.image.area().height as f32,
        );
        self.image.subimage(rect)
    }
}

impl Row {
    fn compact(&mut self) {
        *self = Row::Compacting {
            spaces: self.spaces().clone(),

            time: 0.0,
        };
    }

    fn is_visible(&self) -> bool {
        match self {
            Row::Retracting { .. } => false,
            _ => true,
        }
    }

    fn spaces(&self) -> &Vec<Space> {
        match self {
            Row::Incomplete(spaces) => spaces,
            Row::Compacting { spaces, .. } => spaces,
            Row::Retracting { spaces, .. } => spaces,
        }
    }

    fn spaces_mut(&mut self) -> &mut Vec<Space> {
        match self {
            Row::Incomplete(spaces) => spaces,
            Row::Compacting { spaces, .. } => spaces,
            Row::Retracting { spaces, .. } => spaces,
        }
    }

    fn scale(&self) -> Option<f32> {
        match self {
            Row::Incomplete(_) => None,
            Row::Compacting { time, .. } => Some(lerp(
                1.0,
                1.0 / TETRIS_WIDTH as f32,
                ezing::cubic_out(time / COMPACT_TIME),
            )),
            Row::Retracting { time, .. } => Some(lerp(
                1.0 / TETRIS_WIDTH as f32,
                1.0,
                ezing::quint_inout(time / RETRACT_TIME),
            )),
        }
    }

    fn update(&mut self, dt: f32) {
        let retract = match self {
            Row::Compacting { time, .. } => {
                *time += dt;
                *time > COMPACT_TIME
            }
            Row::Retracting { time, .. } => {
                *time += dt;
                false
            }
            _ => false,
        };

        if retract {
            *self = Row::Retracting {
                spaces: self.spaces().clone(),
                time: 0.0,
            };
        }
    }

    fn done(&self) -> bool {
        match self {
            Row::Retracting { time, .. } => *time >= RETRACT_TIME,
            _ => false,
        }
    }
}

impl Space {
    fn is_filled(&self) -> bool {
        match self {
            Space::Filled { .. } => true,
            Space::Empty => false,
        }
    }
}

impl TetrisShape {
    fn graphics(
        &self,
        rot: u8,
    ) -> (usize, (u8, u8), [[Option<(u8, u8)>; 4]; 4]) {
        let size = match self.0 {
            0 => (4, 1),
            1 => (3, 2),
            2 => (3, 2),
            3 => (2, 2),
            4 => (3, 2),
            5 => (3, 2),
            6 => (3, 2),
            _ => unreachable!(),
        };

        let y = |x, y| Some((x, y));
        let n = None;

        let grid = match (self.0, rot) {
            // I
            (0, 0) => [
                [n, n, n, n],
                [y(0, 0), y(1, 0), y(2, 0), y(3, 0)],
                [n, n, n, n],
                [n, n, n, n],
            ],
            (0, 1) => [
                [n, n, y(0, 0), n],
                [n, n, y(1, 0), n],
                [n, n, y(2, 0), n],
                [n, n, y(3, 0), n],
            ],
            (0, 2) => [
                [n, n, n, n],
                [n, n, n, n],
                [y(3, 0), y(2, 0), y(1, 0), y(0, 0)],
                [n, n, n, n],
            ],
            (0, 3) => [
                [n, y(3, 0), n, n],
                [n, y(2, 0), n, n],
                [n, y(1, 0), n, n],
                [n, y(0, 0), n, n],
            ],

            // J
            (1, 0) => [
                [y(0, 0), n, n, n],
                [y(0, 1), y(1, 1), y(2, 1), n],
                [n, n, n, n],
                [n, n, n, n],
            ],
            (1, 1) => [
                [n, y(0, 1), y(0, 0), n],
                [n, y(1, 1), n, n],
                [n, y(2, 1), n, n],
                [n, n, n, n],
            ],
            (1, 2) => [
                [n, n, n, n],
                [y(2, 1), y(1, 1), y(0, 1), n],
                [n, n, y(0, 0), n],
                [n, n, n, n],
            ],
            (1, 3) => [
                [n, y(2, 1), n, n],
                [n, y(1, 1), n, n],
                [y(0, 0), y(0, 1), n, n],
                [n, n, n, n],
            ],

            // L
            (2, 0) => [
                [n, n, y(2, 0), n],
                [y(0, 1), y(1, 1), y(2, 1), n],
                [n, n, n, n],
                [n, n, n, n],
            ],
            (2, 1) => [
                [n, y(0, 1), n, n],
                [n, y(1, 1), n, n],
                [n, y(2, 1), y(2, 0), n],
                [n, n, n, n],
            ],
            (2, 2) => [
                [n, n, n, n],
                [y(2, 1), y(1, 1), y(0, 1), n],
                [y(2, 0), n, n, n],
                [n, n, n, n],
            ],
            (2, 3) => [
                [y(2, 0), y(2, 1), n, n],
                [n, y(1, 1), n, n],
                [n, y(0, 1), n, n],
                [n, n, n, n],
            ],

            // O
            (3, 0) => [
                [n, y(0, 0), y(1, 0), n],
                [n, y(0, 1), y(1, 1), n],
                [n, n, n, n],
                [n, n, n, n],
            ],
            (3, 1) => [
                [n, y(0, 1), y(0, 0), n],
                [n, y(1, 1), y(1, 0), n],
                [n, n, n, n],
                [n, n, n, n],
            ],
            (3, 2) => [
                [n, y(1, 1), y(0, 1), n],
                [n, y(1, 0), y(0, 0), n],
                [n, n, n, n],
                [n, n, n, n],
            ],
            (3, 3) => [
                [n, y(1, 0), y(1, 1), n],
                [n, y(0, 0), y(0, 1), n],
                [n, n, n, n],
                [n, n, n, n],
            ],

            // S
            (4, 0) => [
                [n, y(1, 0), y(2, 0), n],
                [y(0, 1), y(1, 1), n, n],
                [n, n, n, n],
                [n, n, n, n],
            ],
            (4, 1) => [
                [n, y(0, 1), n, n],
                [n, y(1, 1), y(1, 0), n],
                [n, n, y(2, 0), n],
                [n, n, n, n],
            ],
            (4, 2) => [
                [n, n, n, n],
                [n, y(1, 1), y(0, 1), n],
                [y(2, 0), y(1, 0), n, n],
                [n, n, n, n],
            ],
            (4, 3) => [
                [y(2, 0), n, n, n],
                [y(1, 0), y(1, 1), n, n],
                [n, y(0, 1), n, n],
                [n, n, n, n],
            ],

            // T
            (5, 0) => [
                [n, y(1, 0), n, n],
                [y(0, 1), y(1, 1), y(2, 1), n],
                [n, n, n, n],
                [n, n, n, n],
            ],
            (5, 1) => [
                [n, y(0, 1), n, n],
                [n, y(1, 1), y(1, 0), n],
                [n, y(2, 1), n, n],
                [n, n, n, n],
            ],
            (5, 2) => [
                [n, n, n, n],
                [y(2, 1), y(1, 1), y(0, 1), n],
                [n, y(1, 0), n, n],
                [n, n, n, n],
            ],
            (5, 3) => [
                [n, y(2, 1), n, n],
                [y(1, 0), y(1, 1), n, n],
                [n, y(0, 1), n, n],
                [n, n, n, n],
            ],

            // Z
            (6, 0) => [
                [y(0, 0), y(1, 0), n, n],
                [n, y(1, 1), y(2, 1), n],
                [n, n, n, n],
                [n, n, n, n],
            ],
            (6, 1) => [
                [n, n, y(0, 0), n],
                [n, y(1, 1), y(1, 0), n],
                [n, y(2, 1), n, n],
                [n, n, n, n],
            ],
            (6, 2) => [
                [n, n, n, n],
                [y(2, 1), y(1, 1), n, n],
                [n, y(1, 0), y(0, 0), n],
                [n, n, n, n],
            ],
            (6, 3) => [
                [n, n, y(2, 1), n],
                [n, y(1, 0), y(1, 1), n],
                [n, y(0, 0), n, n],
                [n, n, n, n],
            ],

            (_, _) => unreachable!(),
        };

        (self.0 as usize, size, grid)
    }

    fn shape(&self, rot: u8) -> [[bool; 4]; 4] {
        let y = true;
        let n = false;
        match (self.0, rot) {
            // I
            (0, 0) => [[n, n, n, n], [y, y, y, y], [n, n, n, n], [n, n, n, n]],
            (0, 1) => [[n, n, y, n], [n, n, y, n], [n, n, y, n], [n, n, y, n]],
            (0, 2) => [[n, n, n, n], [n, n, n, n], [y, y, y, y], [n, n, n, n]],
            (0, 3) => [[n, y, n, n], [n, y, n, n], [n, y, n, n], [n, y, n, n]],

            // J
            (1, 0) => [[y, n, n, n], [y, y, y, n], [n, n, n, n], [n, n, n, n]],
            (1, 1) => [[n, y, y, n], [n, y, n, n], [n, y, n, n], [n, n, n, n]],
            (1, 2) => [[n, n, n, n], [y, y, y, n], [n, n, y, n], [n, n, n, n]],
            (1, 3) => [[n, y, n, n], [n, y, n, n], [y, y, n, n], [n, n, n, n]],

            // L
            (2, 0) => [[n, n, y, n], [y, y, y, n], [n, n, n, n], [n, n, n, n]],
            (2, 1) => [[n, y, n, n], [n, y, n, n], [n, y, y, n], [n, n, n, n]],
            (2, 2) => [[n, n, n, n], [y, y, y, n], [y, n, n, n], [n, n, n, n]],
            (2, 3) => [[y, y, n, n], [n, y, n, n], [n, y, n, n], [n, n, n, n]],

            // O
            (3, _) => [[n, y, y, n], [n, y, y, n], [n, n, n, n], [n, n, n, n]],

            // S
            (4, 0) => [[n, y, y, n], [y, y, n, n], [n, n, n, n], [n, n, n, n]],
            (4, 1) => [[n, y, n, n], [n, y, y, n], [n, n, y, n], [n, n, n, n]],
            (4, 2) => [[n, n, n, n], [n, y, y, n], [y, y, n, n], [n, n, n, n]],
            (4, 3) => [[y, n, n, n], [y, y, n, n], [n, y, n, n], [n, n, n, n]],

            // T
            (5, 0) => [[n, y, n, n], [y, y, y, n], [n, n, n, n], [n, n, n, n]],
            (5, 1) => [[n, y, n, n], [n, y, y, n], [n, y, n, n], [n, n, n, n]],
            (5, 2) => [[n, n, n, n], [y, y, y, n], [n, y, n, n], [n, n, n, n]],
            (5, 3) => [[n, y, n, n], [y, y, n, n], [n, y, n, n], [n, n, n, n]],

            // Z
            (6, 0) => [[y, y, n, n], [n, y, y, n], [n, n, n, n], [n, n, n, n]],
            (6, 1) => [[n, n, y, n], [n, y, y, n], [n, y, n, n], [n, n, n, n]],
            (6, 2) => [[n, n, n, n], [y, y, n, n], [n, y, y, n], [n, n, n, n]],
            (6, 3) => [[n, n, y, n], [n, y, y, n], [n, y, n, n], [n, n, n, n]],

            (_, _) => unreachable!(),
        }
    }

    fn robot_collision(&self) -> [[bool; 4]; 4] {
        let y = true;
        let n = false;
        match self.0 {
            // I
            0 => [[n, n, n, n], [y, y, y, y], [n, n, n, n], [n, n, n, n]],

            // J
            1 => [[n, y, n, n], [n, y, n, n], [y, y, n, n], [n, n, n, n]],

            // L
            2 => [[y, n, n, n], [y, n, n, n], [y, y, n, n], [n, n, n, n]],

            // O
            3 => [[y, y, n, n], [y, y, n, n], [n, n, n, n], [n, n, n, n]],

            // S
            4 => [[n, y, y, n], [y, y, n, n], [n, n, n, n], [n, n, n, n]],

            // T
            5 => [[y, y, y, n], [n, y, n, n], [n, n, n, n], [n, n, n, n]],

            // Z
            6 => [[y, y, n, n], [n, y, y, n], [n, n, n, n], [n, n, n, n]],

            _ => unreachable!(),
        }
    }
}

impl TetrisPiece {
    fn blocks(&self) -> Vec<(i32, i32)> {
        let shape = self.shape.shape(self.rot);
        shape
            .iter()
            .enumerate()
            .flat_map(|(y, row)| {
                row.iter().enumerate().filter_map(move |(x, &block)| {
                    if block {
                        Some((self.x + x as i32 - 2, self.y + y as i32 - 2))
                    } else {
                        None
                    }
                })
                // don't have time to make this work correctly
            }).collect::<Vec<_>>()
    }

    fn graphics_spaces(&self) -> Vec<(i32, i32, Space)> {
        let (image, size, shape) = self.shape.graphics(self.rot);
        shape
            .iter()
            .enumerate()
            .flat_map(|(y, row)| {
                row.iter().enumerate().filter_map(move |(x, &block)| {
                    if let Some((ix, iy)) = block {
                        Some((
                            self.x + x as i32 - 2,
                            self.y + y as i32 - 2,
                            Space::Filled {
                                rot: self.rot,
                                image,
                                size,
                                index: (ix, iy),
                            },
                        ))
                    } else {
                        None
                    }
                })
                // don't have time to make this work correctly
            }).collect::<Vec<_>>()
    }
}

impl Robot {
    fn new(angle: f32, distance: f32) -> Robot {
        let direction = Vector::from_angle(angle);
        let position = PLAYER_POS - direction * distance;
        let image = thread_rng().gen_range(0, 7);
        Robot {
            position,
            velocity: direction,
            angle,
            image,
            time: 0.0,
            treads: if image == 3 {
                Some(thread_rng().gen())
            } else {
                None
            },
            frame: thread_rng().gen(),
        }
    }

    fn contains(&self, vec: Vector) -> bool {
        let shape = TetrisShape(self.image as u8);
        let grid = shape.robot_collision();
        for x in 0..4 {
            for y in 0..4 {
                if grid[y][x] {
                    let x = x as f32 * 16.0 + self.position.x;
                    let y = y as f32 * 16.0 + self.position.y;
                    let rect = Rectangle::new(x, y, 16.0, 16.0);
                    if rect.contains(vec) {
                        return true;
                    }
                }
            }
        }
        false
    }

    fn contains_rect(&self, vec: Rectangle) -> bool {
        let shape = TetrisShape(self.image as u8);
        let grid = shape.robot_collision();
        for x in 0..4 {
            for y in 0..4 {
                if grid[y][x] {
                    let x = x as f32 * 16.0 + self.position.x;
                    let y = y as f32 * 16.0 + self.position.y;
                    let rect = Rectangle::new(x, y, 16.0, 16.0);
                    if rect.overlaps_rect(vec) {
                        return true;
                    }
                }
            }
        }
        false
    }

    fn update(&mut self, dt: f32, out_of_space: f32) {
        self.time += dt;
        self.position += self.velocity
            * dt
            * lerp(
                ROBOT_SPEED_MAX,
                ROBOT_SPEED,
                ezing::circ_out((self.time / ROBOT_SLOW_TIME).min(1.0)),
            )
            * lerp(
                1.0,
                60.0,
                ezing::expo_in((out_of_space / ROBOT_OUT_OF_SPACE).min(1.0)),
            );
    }
}

impl GameState {
    fn is_playing(&self) -> bool {
        match self {
            GameState::Playing => true,
            _ => false,
        }
    }

    fn update(&mut self, dt: f32) {
        match self {
            GameState::DeadRobot { time, shake, .. } => {
                *time += dt;
                if *time > SHAKE_START && shake.is_none() {
                    *shake = Some(ScreenShake::new());
                }
                if *time > SHAKE_END && shake.is_some() {
                    *shake = None;
                }

                if let Some(shake) = shake {
                    shake.update(dt, SHAKE_FREQUENCY);
                }
            }
            _ => (),
        }
    }
}

impl PlayState {
    fn is_tetris_at(&self, x: i32, y: i32) -> bool {
        if y < 0 || x < 0 || x >= TETRIS_WIDTH as i32 {
            return true;
        }

        match self.tetris_grid.get(y as usize) {
            Some(ref row) => row.spaces()[x as usize].is_filled(),
            None => false,
        }
    }

    fn gun_shoot_pos(&self) -> Vector {
        let transform = Transform::translate(PLAYER_POS + GUN_OFFSET)
            * Transform::rotate(self.player_angle + 180.0)
            * Transform::translate(
                GUN_CENTER //+ self.gun_image.area().size() * 0.5,
            );
        transform * GUN_SHOOT_OFFSET
    }

    //    fn tetris_at(&self, x: i32, y: i32) -> Space {
    //        if y < 0 {
    //            return Space::Empty;
    //        }
    //
    //        match self.tetris_grid.get(y as usize) {
    //            Some(ref row) => row.spaces()[x as usize],
    //            None => Space::Empty,
    //        }
    //    }

    fn tetris_at_mut(&mut self, x: i32, y: i32) -> &mut Space {
        while self.tetris_grid.len() <= y as usize {
            let mut row = Vec::with_capacity(TETRIS_WIDTH);
            row.resize(TETRIS_WIDTH, Space::Empty);
            self.tetris_grid.push(Row::Incomplete(row));
        }

        &mut self.tetris_grid[y as usize].spaces_mut()[x as usize]
    }

    fn draw_space(
        &self,
        block: &Space,
        x: i32,
        y: i32,
        window: &mut Window,
        transform: Transform,
        ghost: bool,
    ) {
        if let Space::Filled {
            rot,
            image,
            size,
            index,
        } = block
        {
            let image = if ghost {
                &self.ghost_images[*image]
            } else {
                &self.piece_images[*image]
            };

            let (width, height) = (image.area().width, image.area().height);
            let (width, height) =
                (width / size.0 as f32, height / size.1 as f32);
            let rect = Rectangle::new(
                index.0 as f32 * width,
                index.1 as f32 * height,
                width,
                height,
            );
            let image = image.subimage(rect);

            let x = SIZE_L.0 as f32 - (x as f32 + 0.5) * BLOCK_SIZE;
            let y = SIZE_L.1 as f32 - (y as f32 + 0.5) * BLOCK_SIZE;
            let angle = *rot as f32 * 90.0;
            window.draw(
                &Draw::image(
                    &image,
                    Transform::translate(OFFSET_LV)
                        * transform
                        * Vector::new(x, y),
                ).with_transform(
                    transform
                        * Transform::scale(Vector::new(
                            -BLOCK_SIZE as f32 / width,
                            -BLOCK_SIZE as f32 / height,
                        ))
                        * Transform::rotate(angle),
                ),
            );
        }
    }

    fn piece_valid(&self, piece: TetrisPiece) -> bool {
        for (x, y) in piece.blocks() {
            if self.is_tetris_at(x, y) {
                return false;
            }
        }
        true
    }

    fn check_rows(&mut self) {
        let mut rows_crushed = 0;

        for row in self.tetris_grid.iter_mut() {
            let transition = match row {
                Row::Incomplete(ref spaces) => {
                    !spaces.iter().any(|&space| space == Space::Empty)
                }
                _ => false,
            };
            if transition {
                self.mass_consumed += 1;
                rows_crushed += 1;
                row.compact();
            }
        }

        if rows_crushed > 0 {
            self.sound_crush.play();
        }
    }
}

impl State for PlayState {
    fn new() -> PlayState {
        let piece_images = future::join_all(
            [
                "assets/images/I.png",
                "assets/images/J.png",
                "assets/images/L.png",
                "assets/images/O.png",
                "assets/images/S.png",
                "assets/images/T.png",
                "assets/images/Z.png",
            ]
                .into_iter()
                .map(Image::load),
        ).wait()
        .unwrap();
        let ghost_images = future::join_all(
            [
                "assets/images/I_ghost.png",
                "assets/images/J_ghost.png",
                "assets/images/L_ghost.png",
                "assets/images/O_ghost.png",
                "assets/images/S_ghost.png",
                "assets/images/T_ghost.png",
                "assets/images/Z_ghost.png",
            ]
                .into_iter()
                .map(Image::load),
        ).wait()
        .unwrap();
        let background_image =
            Image::load("assets/images/background.png").wait().unwrap();
        let desert_image =
            Image::load("assets/images/desert.png").wait().unwrap();
        let gameover_image =
            Image::load("assets/images/gameover.png").wait().unwrap();
        let numbers_sheet =
            Animation::new("assets/images/numbers-sheet.png", 10, 1.0);

        let robot_anims = vec![
            Animation::new("assets/images/robot_I.png", 12, 0.03),
            Animation::new("assets/images/robot_J.png", 8, 0.05),
            Animation::new("assets/images/robot_L.png", 8, 0.05),
            Animation::new("assets/images/robot_O.png", 8, 0.15),
            Animation::new("assets/images/robot_S.png", 8, 0.05),
            Animation::new("assets/images/robot_T.png", 8, 0.07),
            Animation::new("assets/images/robot_Z.png", 8, 0.05),
        ];
        let tread_anim =
            Animation::new("assets/images/robot_treads.png", 6, 0.1);
        let player_anim = Animation::new("assets/images/player.png", 24, 0.075);
        let gun_anim = Animation::new("assets/images/gun.png", 8, 0.14);
        let gun_stop =
            Image::load("assets/images/gun_stopped.png").wait().unwrap();
        let sound_crush =
            Sound::load("assets/sounds/crush.wav").wait().unwrap();
        let sound_gameover =
            Sound::load("assets/sounds/gameover.wav").wait().unwrap();
        let squisher =
            Image::load("assets/images/squisher.png").wait().unwrap();

        let mut row = Vec::with_capacity(TETRIS_WIDTH);
        row.resize(TETRIS_WIDTH, Space::Empty);

        PlayState {
            player_direction: Vector::new(0, 1),
            player_angle: 0.0,
            last_tick: Instant::now(),
            next_robot: 0,
            robots: HashMap::new(),
            robot_spawn_timer: 0.0,
            tetris_grid: vec![Row::Incomplete(row)],
            tetris_move_timer: TETRIS_MOVE_INTERVAL,
            tetris_piece: None,
            lasers: Vec::new(),
            out_of_space: 0.0,
            bots_beaned: 0,
            mass_consumed: 0,

            piece_images,
            ghost_images,
            sound_crush,
            sound_gameover,
            robot_anims,
            tread_anim,
            player_anim,
            gun_anim,
            gun_stop,
            squisher,
            fast_held: false,
            background_image,
            desert_image,
            gameover_image,
            numbers_sheet,

            state: GameState::Playing,
        }
    }

    fn event(&mut self, event: &Event, _window: &mut Window) {
        match event {
            Event::Key(Key::R, ButtonState::Pressed) => {
                let mut row = Vec::with_capacity(TETRIS_WIDTH);
                row.resize(TETRIS_WIDTH, Space::Empty);
                self.next_robot = 0;
                self.robots.clear();
                self.robot_spawn_timer = 0.0;
                self.tetris_grid = vec![Row::Incomplete(row)];
                self.tetris_move_timer = TETRIS_MOVE_INTERVAL;
                self.tetris_piece = None;
                self.lasers = Vec::new();
                self.out_of_space = 0.0;
                self.state = GameState::Playing;
                self.fast_held = false;
                self.bots_beaned = 0;
                self.mass_consumed = 0;
            }
            _ => (),
        }

        if self.state.is_playing() {
            match event {
                Event::MouseMoved(pos) => {
                    self.player_direction =
                        (PLAYER_POS + OFFSET_RV - *pos).normalize();
                    self.player_angle = self.player_direction.angle();
                }
                Event::MouseButton(MouseButton::Left, ButtonState::Pressed) => {
                    if self.tetris_piece.is_none()
                        && self.tetris_grid.len() < TETRIS_HEIGHT
                    {
                        let shoot_from = self.gun_shoot_pos();
                        // budget raycasting™
                        let mut collision = None;
                        let mut sample = shoot_from;
                        let whole_screen =
                            Rectangle::new(0, 0, SIZE_R.0, SIZE_R.1);
                        let small_number = 0.05;

                        'outer: while whole_screen.contains(sample) {
                            sample -= self.player_direction * small_number;
                            for (&idx, robot) in self.robots.iter() {
                                if robot.contains(sample) {
                                    collision = Some(idx);
                                    break 'outer;
                                }
                            }
                        }

                        if let Some(idx) = collision {
                            let robot = self.robots.remove(&idx).unwrap();
                            self.tetris_piece = Some(TetrisPiece {
                                x: TETRIS_WIDTH as i32 / 2,
                                y: TETRIS_HEIGHT as i32,
                                rot: thread_rng().gen_range(0, 4),
                                shape: TetrisShape(robot.image as u8),
                            });
                            self.bots_beaned += 1;
                        }
                        self.lasers.push(Laser {
                            from: shoot_from,
                            to: sample,
                            time: 0.0,
                        });
                    }
                }
                Event::Key(Key::S, ButtonState::Pressed) => {
                    self.fast_held = true
                }
                Event::Key(Key::S, ButtonState::Released) => {
                    self.fast_held = false
                }
                Event::Key(Key::Space, ButtonState::Pressed) => {
                    // hard drop
                    if let Some(mut piece) = self.tetris_piece {
                        while self.piece_valid(piece) {
                            piece.y -= 1;
                        }

                        piece.y += 1;

                        for (x, y, space) in piece.graphics_spaces() {
                            *self.tetris_at_mut(x, y) = space;
                        }

                        self.check_rows();
                        self.fast_held = false;
                        self.tetris_piece = None;
                    }
                }
                Event::Key(Key::A, ButtonState::Pressed) => {
                    if let Some(mut piece) = self.tetris_piece {
                        piece.x += 1;
                        if self.piece_valid(piece) {
                            self.tetris_piece = Some(piece);
                        }
                    }
                }
                Event::Key(Key::D, ButtonState::Pressed) => {
                    if let Some(mut piece) = self.tetris_piece {
                        piece.x -= 1;
                        if self.piece_valid(piece) {
                            self.tetris_piece = Some(piece);
                        }
                    }
                }
                Event::Key(Key::W, ButtonState::Pressed) => {
                    if let Some(mut piece) = self.tetris_piece {
                        piece.rot = (piece.rot + 1) % 4;
                        if self.piece_valid(piece) {
                            self.tetris_piece = Some(piece);
                        }
                    }
                }
                Event::Key(Key::LControl, ButtonState::Pressed) => {
                    if let Some(mut piece) = self.tetris_piece {
                        piece.rot = (piece.rot + 4 - 1) % 4;
                        if self.piece_valid(piece) {
                            self.tetris_piece = Some(piece);
                        }
                    }
                }
                _ => (),
            }
        }
    }

    fn update(&mut self, _window: &mut Window) {
        let now = Instant::now();
        let dt = now - self.last_tick;
        let dt = dt.subsec_nanos() as f32 * 1e-9 + dt.as_secs() as f32;
        self.last_tick = now;

        for anim in self.robot_anims.iter_mut() {
            anim.update(dt);
        }
        self.tread_anim.update(dt);
        self.player_anim.update(dt);
        self.gun_anim.update(dt);

        self.state.update(dt);

        match self.state.clone() {
            GameState::Playing => {
                if self.tetris_grid.len() >= TETRIS_HEIGHT {
                    self.out_of_space += dt;
                }

                for laser in self.lasers.iter_mut() {
                    laser.time += dt;
                }
                self.lasers.retain(|laser| laser.time < LASER_DURATION);

                for robot in self.robots.values_mut() {
                    robot.update(dt, self.out_of_space);
                }

                // check for collisions with the player
                let player_rect =
                    Rectangle::newv(PLAYER_POS, Vector::new(16, 16));
                for robot in self.robots.values() {
                    if robot.contains_rect(player_rect) {
                        self.sound_gameover.play();
                        self.state = GameState::DeadRobot {
                            time: 0.0,
                            pos: robot.position
                                + self.robot_anims[robot.image]
                                    .get_frame(robot.frame)
                                    .area()
                                    .size()
                                    * 0.5,
                            shake: None,
                        };
                    }
                }

                self.robot_spawn_timer -= dt;
                while self.robot_spawn_timer < 0.0 {
                    let mut rng = thread_rng();
                    self.robot_spawn_timer += ROBOT_SPAWN_INTERVAL;
                    let num =
                        rng.gen_range(ROBOT_WAVE_SIZE.0, ROBOT_WAVE_SIZE.1);
                    for _ in 0..num {
                        let angle = rng.gen_range(0.0, 360.0);
                        let distance = rng.gen_range(
                            ROBOT_SPAWN_DISTANCE.0,
                            ROBOT_SPAWN_DISTANCE.1,
                        );
                        self.robots.insert(
                            self.next_robot,
                            Robot::new(angle, distance),
                        );
                        self.next_robot += 1;
                    }
                }

                if self.fast_held {
                    self.tetris_move_timer -= dt * TIMESCALE_FAST;
                } else {
                    self.tetris_move_timer -= dt;
                }
                while self.tetris_move_timer < 0.0 {
                    self.tetris_move_timer += TETRIS_MOVE_INTERVAL;

                    if let Some(mut piece) = self.tetris_piece.clone() {
                        piece.y -= 1;
                        if !self.piece_valid(piece) {
                            // move on top of the other pieces
                            while !self.piece_valid(piece) {
                                piece.y += 1;
                            }

                            for (x, y, space) in piece.graphics_spaces() {
                                *self.tetris_at_mut(x, y) = space;
                            }

                            self.check_rows();
                            self.fast_held = false;

                            self.tetris_piece = None;
                        } else {
                            self.tetris_piece = Some(piece)
                        }
                    }
                }

                for row in self.tetris_grid.iter_mut() {
                    row.update(dt);
                }

                self.tetris_grid.retain(|row| !row.done());
            }
            _ => (),
        }
    }

    fn draw(&mut self, window: &mut Window) {
        window.clear(Color::black());

        // zoom in on the player, if died to a robot
        match &self.state {
            GameState::Playing => {
                window
                    .set_view(View::new(Rectangle::new(0, 0, SIZE.0, SIZE.1)));
            }
            GameState::DeadRobot { pos, time, shake } => {
                let scale = lerp(
                    1.0,
                    10.0,
                    ezing::elastic_out((time / ROBOT_DEATH_TIME).min(1.0)),
                );
                let pos =
                    *pos + OFFSET_RV - Vector::new(SIZE.0 / 2, SIZE.1 / 2);
                let mut transform = Transform::translate(pos)
                    * Transform::scale(Vector::new(scale, scale))
                    * Transform::translate(-pos);

                // screenshake
                if let Some(shake) = shake {
                    let intensity =
                        // fade in
                        ezing::circ_out(((*time - SHAKE_START) / SHAKE_FADE_IN).min(1.0)) -
                        // fade out
                        ezing::quad_in(((*time - SHAKE_END + SHAKE_FADE_OUT) /
                     SHAKE_FADE_OUT).min(1.0).max(0.0));

                    let (offset, rot) = shake.offset(SHAKE_FREQUENCY);
                    transform = Transform::translate(
                        offset * SHAKE_INTENSITY * intensity,
                    ) * Transform::rotate(
                        rot * SHAKE_INTENSITY_ROT * intensity,
                    ) * transform;
                }

                window.set_view(View::new_transformed(
                    Rectangle::new(0, 0, SIZE.0, SIZE.1),
                    transform,
                ));
            }
        }

        window.draw(
            &Draw::image(
                &self.desert_image,
                Vector::new(SIZE.0 / 2, SIZE.1 / 2),
            ).with_z(-50.0),
        );
        window.draw(
            &Draw::image(
                &self.background_image,
                Vector::new(SIZE.0 / 2, SIZE.1 / 2),
            ).with_z(-10.0),
        );

        // draw left hand side

        // draw current piece
        if let Some(ref piece) = self.tetris_piece {
            for (x, y, space) in piece.graphics_spaces() {
                self.draw_space(
                    &space,
                    x,
                    y,
                    window,
                    Transform::identity(),
                    false,
                );
            }

            // draw ghost at the bottom
            let mut piece = piece.clone();
            while self.piece_valid(piece) {
                piece.y -= 1;
            }
            piece.y += 1;
            for (x, y, space) in piece.graphics_spaces() {
                self.draw_space(
                    &space,
                    x,
                    y,
                    window,
                    Transform::identity(),
                    true,
                );
            }
        }
        // draw grid
        for (y, row) in self.tetris_grid.iter().enumerate() {
            let transform = match row.scale() {
                Some(scale) => {
                    let image = self.squisher.subimage(Rectangle::newv(
                        Vector::zero(),
                        Vector::new(
                            (1.0 - scale) * SIZE_L.0 as f32 / 2.0,
                            self.squisher.area().height,
                        ),
                    ));

                    let squish_offset = Vector::new(
                        SIZE_L.0 as f32 - (1.0 - scale) * SIZE_L.0 as f32 / 2.0,
                        SIZE_L.1 as f32 - (y as f32 + 0.5) * BLOCK_SIZE,
                    ) + Vector::new(
                        image.area().width / 2.0,
                        0.0,
                    ) + OFFSET_LV;
                    window
                        .draw(&Draw::image(&image, squish_offset).with_z(-0.5));

                    let squish_offset =
                        Vector::new(
                            (1.0 - scale) * SIZE_L.0 as f32 / 2.0,
                            SIZE_L.1 as f32 - (y as f32 + 0.5) * BLOCK_SIZE,
                        ) - Vector::new(image.area().width / 2.0, 0.0)
                            + OFFSET_LV;

                    window.draw(
                        &Draw::image(&image, squish_offset)
                            .with_transform(Transform::scale(Vector::new(
                                -1.0, 1.0,
                            ))).with_z(-0.5),
                    );

                    Transform::translate(Vector::new(
                        (1.0 - scale) * SIZE_L.0 as f32 / 4.0,
                        0.0,
                    )) * Transform::scale(Vector::new(scale, 1.0))
                }
                None => Transform::identity(),
            };

            if row.is_visible() {
                for (x, space) in row.spaces().iter().enumerate() {
                    if let Space::Filled { .. } = space {
                        self.draw_space(
                            space, x as i32, y as i32, window, transform,
                            false,
                        );
                    }
                }
            }
        }

        // draw right hand side
        //window.draw(
        //    &Draw::rectangle(Rectangle::newv(
        //        OFFSET_RV,
        //        Vector::new(SIZE_R.0, SIZE_R.1),
        //    )).with_color(rgb(204, 173, 128)),
        //);

        // draw player
        let image = self.player_anim.get_frame(0);
        window.draw(&Draw::image(&image, PLAYER_POS + OFFSET_RV));

        // draw player gun
        let image = if self.tetris_piece.is_none()
            && self.tetris_grid.len() < TETRIS_HEIGHT
        {
            self.gun_anim.get_frame(0)
        } else {
            self.gun_stop.clone()
        };
        window.draw(&Draw::image(&image, Vector::zero()).with_transform(
            Transform::translate(
                PLAYER_POS + OFFSET_RV + GUN_OFFSET, //- GUN_CENTER * 2.0
            ) * Transform::rotate(self.player_angle + 180.0)
                * Transform::translate(GUN_CENTER + image.area().size() * 0.5)
                * if self.player_angle > 90.0 || self.player_angle < -90.0 {
                    Transform::identity()
                } else {
                    Transform::scale(Vector::new(1.0, -1.0))
                },
        ));

        // draw robots
        for robot in self.robots.values() {
            if robot.position.x < -ROBOT_RADIUS {
                continue;
            }

            let image = &self.robot_anims[robot.image].get_frame(robot.frame);

            if let Some(frame) = robot.treads {
                let tread_image = &self.tread_anim.get_frame(frame);
                window.draw(
                    &Draw::image(
                        tread_image,
                        robot.position + OFFSET_RV + image.area().size() * 0.5,
                    ).with_transform(Transform::rotate(robot.angle + 90.0))
                    .with_z(-10.2),
                );
            }

            window.draw(
                &Draw::image(
                    image,
                    robot.position + OFFSET_RV + image.area().size() * 0.5,
                ).with_z(-10.1),
            );
        }

        // draw lasers
        for laser in self.lasers.iter() {
            let alpha =
                lerp(0.8, 0.0, ezing::back_in(laser.time / LASER_DURATION));
            // for some reason I can't draw lines
            let pos = laser.from;
            let angle = (laser.to - pos).angle();
            let len = (laser.to - pos).len();
            window.draw(
                &Draw::rectangle(Rectangle::new_sized(len, LASER_WIDTH))
                    .with_transform(
                        Transform::translate(pos + OFFSET_RV)
                            * Transform::translate(Vector::new(
                                -len / 2.0,
                                -LASER_WIDTH / 2.0,
                            ))
                            * Transform::rotate(angle)
                            * Transform::translate(Vector::new(
                                len / 2.0,
                                LASER_WIDTH / 2.0,
                            )),
                    ).with_color(rgb(254, 13, 31).with_alpha(alpha))
                    .with_z(-10.01),
            );
        }

        // draw score counters

        // mass consumed: (284, 299)
        let num = format!("{}", self.mass_consumed);
        for (i, digit) in num.chars().enumerate() {
            let digit = digit.to_digit(10).unwrap();
            let image = self.numbers_sheet.get_frame(digit as usize);
            let x = 284.0 + i as f32 * (image.area().width + 1.0);
            let y = 300.0 - image.area().height * 0.5;
            window.draw(&Draw::image(&image, Vector::new(x, y)).with_z(50.0));
        }
        // bots beaned: (426, 27)
        let num = format!("{}", self.bots_beaned);
        for (i, digit) in num.chars().enumerate() {
            let digit = digit.to_digit(10).unwrap();
            let image = self.numbers_sheet.get_frame(digit as usize);
            let x = 426.0 + i as f32 * (image.area().width + 1.0);
            let y = 28.0 - image.area().height * 0.5;
            window.draw(&Draw::image(&image, Vector::new(x, y)).with_z(50.0));
        }

        if let GameState::DeadRobot { time, .. } = &self.state {
            if *time > FADE_OUT_START {
                let alpha = lerp(
                    0.0,
                    1.0,
                    ezing::sine_in(
                        ((*time - FADE_OUT_START) / FADE_OUT_TIME).min(1.0),
                    ),
                );
                window
                    .set_view(View::new(Rectangle::new(0, 0, SIZE.0, SIZE.1)));
                window.draw(
                    &Draw::image(
                        &self.gameover_image,
                        Vector::new(SIZE.0 / 2, SIZE.1 / 2),
                    ).with_color(rgb(255, 255, 255).with_alpha(alpha))
                    .with_z(100.0),
                );
            }
        }

        window.present();
    }
}

fn main() {
    run::<PlayState>(
        WindowBuilder::new("LD42", SIZE.0, SIZE.1)
            .with_resize_strategy(ResizeStrategy::Fit)
            .with_scaling_strategy(ImageScaleStrategy::Pixelate),
    );
}
